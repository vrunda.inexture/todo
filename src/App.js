import { useState } from 'react';
import TaskList from './components/TaskList';
import './App.css';

const App = () => {
  const [tasks, setTasks] = useState([]);
  const [taskTitle, setTaskTitle] = useState('');
  const [filter, setFilter] = useState('all');

  const addTask = () => {
    if (taskTitle !== '') {
      let newTask = { title: taskTitle, completed: false };
      setTasks([...tasks, newTask]);
      setTaskTitle('');
    }
  };

  const changeStatus = (index) => {
    let currentTasks = tasks;
    currentTasks[index].completed = !currentTasks[index].completed;
    setTasks([...currentTasks]);
  };

  const deleteTask = (index) => {
    let currentTasks = tasks;
    currentTasks.splice(index, 1);
    setTasks([...currentTasks]);
  };

  return (
    <div className='text-center'>
      <h1>Work To Be Done</h1>
      <hr />
      <div className='input-group mb-5 input-main'>
        <input
          type='text'
          className='form-control'
          placeholder='Add a task...'
          value={taskTitle}
          onChange={(e) => setTaskTitle(e.target.value)}
        />

        <button
          className='btn input-group-text text-light bg-light'
          onClick={addTask}
        >
          <span className='btn-custom'>
            <i className='fa fa-plus'></i>
          </span>
        </button>

        <select
          className='ms-3'
          value={filter}
          onChange={(e) => setFilter(e.target.value)}
        >
          <option value='all'>All</option>
          <option value='completed'>Completed</option>
          <option value='incomplete'>Incomplete</option>
        </select>
      </div>

      {tasks.map((value, index) => {
        if (filter === 'all') {
          return (
            <TaskList
              key={index}
              value={value}
              index={index}
              changeStatus={changeStatus}
              deleteTask={deleteTask}
            />
          );
        } else if (filter === 'completed') {
          return (
            value.completed && (
              <TaskList
                key={index}
                value={value}
                index={index}
                changeStatus={changeStatus}
                deleteTask={deleteTask}
              />
            )
          );
        } else {
          return (
            !value.completed && (
              <TaskList
                key={index}
                value={value}
                index={index}
                changeStatus={changeStatus}
                deleteTask={deleteTask}
              />
            )
          );
        }
      })}
    </div>
  );
};

export default App;
