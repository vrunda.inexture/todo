import React from 'react';
import PropTypes from 'prop-types';

const TaskList = ({ value, index, changeStatus, deleteTask }) => {
  return (
    <div
      className={`input-group mb-5 task-list ${
        value.completed ? 'completed' : ''
      }`}
      key={index}
    >
      <input
        type='text'
        className='form-control'
        value={value.title}
        disabled
      />
      <button
        className='btn input-group-text text-light btn-success'
        onClick={() => changeStatus(index)}
      >
        <span>
          <i className='fa fa-check'></i>
        </span>
      </button>
      <button
        className='btn input-group-text text-light btn-custom'
        onClick={() => deleteTask(index)}
      >
        <span>
          <i className='fa fa-trash'></i>
        </span>
      </button>
    </div>
  );
};

TaskList.propTypes = {
  value: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  changeStatus: PropTypes.func.isRequired,
  deleteTask: PropTypes.func.isRequired,
};

export default TaskList;
